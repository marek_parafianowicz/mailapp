require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mailapp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    ActionMailer::Base.smtp_settings = {
	    :address => "smtp.sparkpostmail.com",
	    # :domain => "mail.google.com",
	    :port => 587,
	    :user_name => "SMTP_Injection",
	    :password => "adff9549d8ee215ec002da5b2dca2296f35ee9d3",
	    :enable_starttls_auto => true,
	    :authentication => "AUTH LOGIN"
	}
  end
end
