# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name: "User", 
			 email: "example@domain.com", 
			 password: "password", 
			 password_confirmation: "password")

20.times do |n|
	name = Faker::Name.name
	email = "example-#{n}@domain.com"
	password = "password"
	User.create!(name: name, 
				 email: email, 
				 password: password, 
				 password_confirmation: password)
end

users = User.order(:created_at)
90.times do |n|
	title = Faker::Lorem.sentence(3, true, 3)
	content = Faker::Lorem.sentence
	users.each { |user| user.posts.create!(title: title, content: content)}
end